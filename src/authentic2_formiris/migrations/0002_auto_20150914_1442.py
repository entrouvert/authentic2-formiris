# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_formiris', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profileupdate',
            name='updated',
            field=models.DateTimeField(null=True, verbose_name='updated', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='profileupdate',
            name='validated',
            field=models.DateTimeField(null=True, verbose_name='validated', blank=True),
            preserve_default=True,
        ),
    ]
