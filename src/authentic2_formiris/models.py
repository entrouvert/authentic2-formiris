# -*- coding: utf-8 -*-
import random

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from django.conf import settings

from authentic2.utils import send_templated_mail

# put your models here
def get_token():
    return str(random.randint(1, 100000000000))

class ProfileUpdate(models.Model):
    user = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        verbose_name=_('user'),
        unique=True)
    first_name = models.CharField(
        _('first name'),
        max_length=30,
        blank=True)
    last_name = models.CharField(
        _('last name'),
        max_length=30,
        blank=True)
    email = models.EmailField(
        _('email address'),
        blank=True)
    token = models.CharField(
        default=get_token,
        max_length=64,
        verbose_name=_('token'))
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('created'))
    updated = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_('updated'))
    validated = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_('validated'))

    def validate(self):
        if self.validated is None:
            self.user.first_name = self.first_name
            self.user.last_name = self.last_name
            self.user.email = self.email
            self.user.save()
            self.validated = now()
            self.save()
            return True

    def send_mail(self, request):
        ctx = {
            'update': self,
            'base_url': request.build_absolute_uri('/')[:-1],
        }
        send_templated_mail(self.email, 'authentic2_formiris/email', ctx)

    def __unicode__(self):
        return u"utilisateur %s" % self.user

    class Meta:
        verbose_name = _('profile update')
        verbose_name_plural = _('profile updates')
        ordering = ('user__last_name', 'user__first_name')
