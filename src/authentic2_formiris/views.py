import logging
from datetime import timedelta

from django.shortcuts import render
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils.timezone import now

from authentic2.utils import continue_to_next_url, redirect

from . import models, forms

__ALL_ = ['index', 'validate']


def index(request):
    logger = logging.getLogger(__name__)
    try:
        restriction = models.ProfileUpdate.objects.get(user=request.user)
    except models.ProfileUpdate.DoesNotExist:
        return continue_to_next_url(request)
    if restriction.updated is not None and \
       (now() - restriction.updated) >= timedelta(days=7):
        logger.info(u'user blocked because its profile update has not been '
                    'validated in %s days', (now() - restriction.updated).days)
        if request.method == 'POST':
            restriction.send_mail(request)
            logger.info('profile update validation email resent')
            messages.info(request, _('Validation mail sent again'))
        return render(request, 'authentic2_formiris/timeout.html')
    if request.method == 'POST':
        form = forms.UpdateProfileForm(data=request.POST, instance=restriction,
                                       request=request)
        if form.is_valid():
            logger.info(u'profile updated, waiting for validation')
            form.save()
            return continue_to_next_url(request)
    else:
        form = forms.UpdateProfileForm(instance=restriction, request=request)
    return render(request, 'authentic2_formiris/index.html', {'form': form})


def validate(request, token):
    logger = logging.getLogger(__name__)
    try:
        pu = models.ProfileUpdate.objects.get(token=token)
        pu.validate()
    except models.ProfileUpdate.DoesNotExist:
        messages.info(request, _('Dead link, email not validated'))
    else:
        logger.info(u'profile update email %s validated', pu.email)
        messages.info(request, _('Email validated'))
    return redirect(request, 'auth_homepage')
