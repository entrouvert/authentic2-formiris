import logging

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from authentic2 import admin as a2_admin
from . import models


def force_profile_update(modeladmin, request, queryset):
    # delete old ones
    models.ProfileUpdate.objects.filter(user=queryset).delete()
    # create new
    logger = logging.getLogger(__name__)
    for user in queryset:
        models.ProfileUpdate.objects.get_or_create(
            user=user)
        logger.info(u'force update of profile for user %s', user)
force_profile_update.short_description = _('Force profile update')

a2_admin.AuthenticUserAdmin.actions = a2_admin.AuthenticUserAdmin.actions + \
    [force_profile_update]


class ProfileUpdateAdmin(admin.ModelAdmin):
    actions = ['validate', 'send_email']
    list_display = ['user', 'created', 'updated', 'first_name', 'last_name',
                    'email', 'validated']
    search_fields = ['user__email', 'user__last_name', 'user__first_name',
                     'first_name', 'last_name', 'email', 'user__username']
    readonly_fields = ['token']

    def validate(self, request, queryset):
        logger = logging.getLogger(__name__)
        for o in queryset:
            if o.validate():
                logger.info(u'profile update of user %s validated by '
                            'administrator', o.user)
    validate.short_description = _('Validate profile update')

    def send_email(self, request, queryset):
        logger = logging.getLogger(__name__)
        for o in queryset:
            if o.validated is not None:
                continue
            o.send_mail(request)
            logger.info(u'profile update validation email of user %s resent '
                        'by admin', o.user)
    send_email.short_description = _('Resend validation email')

admin.site.register(models.ProfileUpdate, ProfileUpdateAdmin)
