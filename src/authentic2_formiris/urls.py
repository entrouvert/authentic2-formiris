from django.conf.urls import patterns, url

from authentic2.decorators import setting_enabled, required

from . import app_settings
from .views import index, validate

urlpatterns = required(
        setting_enabled('ENABLE', settings=app_settings),
        patterns('',
            url('^accounts/update-profile/$', index,
                name='authentic2-formiris-update-profile'),
            url('^accounts/validate-profile/(?P<token>\d+)/$', validate,
                name='authentic2-formiris-validate-profile'),
        )
)
