from django import forms

from django.contrib.auth import get_user_model
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from . import models

class UpdateProfileForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    first_name = forms.CharField(
        label=_('first name').title(),
        max_length=64)
    last_name = forms.CharField(
        label=_('last name').title(),
        max_length=64)
    email = forms.EmailField(
        label=_('email address').title(),
        max_length=254)
    new_password1 = forms.CharField(label=_("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=_("New password confirmation"),
                                    widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(UpdateProfileForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def save(self, commit=True):
        self.instance.updated = now()
        self.instance.send_mail(self.request)
        self.instance.user.set_password(self.cleaned_data['new_password1'])
        self.instance.user.save()
        return super(UpdateProfileForm, self).save(commit=commit)

    class Meta:
        model = models.ProfileUpdate
        fields = [
            'first_name',
            'last_name',
            'email',
        ]
