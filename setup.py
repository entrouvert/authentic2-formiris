#!/usr/bin/python
import subprocess
import sys
import os

from setuptools import setup, find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management import call_command
            for dir in ('src/authentic2_formiris',):
                for path, dirs, files in os.walk(dir):
                    if 'locale' not in dirs:
                        continue
                    curdir = os.getcwd()
                    os.chdir(os.path.realpath(path))
                    call_command('compilemessages')
                    os.chdir(curdir)
        except ImportError:
            print
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations')
            print
            print


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands

class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)

def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--dirty','--match=v*'],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            return result.split()[0][1:].replace('-', '.')
        else:
            return '0.0.0-%s' % len(
                    subprocess.check_output(
                            ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0.0'

class eo_sdist(sdist):

    def run(self):
        print "creating VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print "removing VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')

README = file(os.path.join(
    os.path.dirname(__file__),
    'README')).read()

setup(name='authentic2-formiris',
        version=get_version(),
        license='AGPLv3',
        description='Authentic2 Formiris',
        long_description=README,
        author="Entr'ouvert",
        author_email="info@entrouvert.com",
        packages=find_packages('src'),
        package_dir={
            '': 'src',
        },
        package_data={
            'authentic2_formiris': [
                  'templates/authentic2_formiris/*.html',
                  'templates/authentic2_formiris/*.txt',
                  'static/authentic2_formiris/js/*.js',
                  'static/authentic2_formiris/css/*.css',
                  'static/authentic2_formiris/img/*.png',
                  'static/authentic2_formiris/img/*.png',
                  'locale/fr/LC_MESSAGES/*.po',
                  'locale/fr/LC_MESSAGES/*.mo',
            ],
        },
        install_requires=[
        ],
        cmdclass={'build': build, 'install_lib': install_lib,
            'compile_translations': compile_translations,
            'sdist': eo_sdist},
        entry_points={
            'authentic2.plugin': [
                'authentic2-formiris= authentic2_formiris:Plugin',
            ],
        },
)
